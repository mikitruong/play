import { useState, useEffect } from 'react'

const useIntersection = (element: Element | null, rootMargin: string) => {
  const [isVisible, setIsVisible] = useState(true)
  useEffect(() => {
    if (element !== null) {
      const observer = new IntersectionObserver(([entry]) => {
        setIsVisible(entry.isIntersecting)
      }, { rootMargin })

      // eslint-disable-next-line no-unused-expressions
      element && observer.observe(element)

      return () => observer.unobserve(element)
    }
    return () => setIsVisible(true)
  }, [element, rootMargin])
  return isVisible
}

export default useIntersection
