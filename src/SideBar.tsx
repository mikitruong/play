import { Paper } from '@mui/material'
import React from 'react'

const SideBar: React.FC = () => {
  return (
    <Paper
      variant="outlined"
      square
      style={{
        borderWidth: 0,
      }}
    />
  )
}

export default SideBar
