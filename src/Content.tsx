import React from 'react'
import { Grid } from '@mui/material'
import EPDChart from './EPDChart'

const Content: React.FC = () => {
  const charts = new Array(30).fill(
    <Grid item xs>
      <EPDChart />
    </Grid>,
  )

  return (
    <Grid
      container
      direction="column"
      justifyContent="space-evenly"
      alignContent="center"
      sx={{
        overflow: 'auto',
        flexWrap: 'nowrap',
      }}
    >
      {charts}
    </Grid>
  )
}

export default Content
