import React from 'react'
import ReactDOM from 'react-dom'
import { CssBaseline } from '@mui/material'
import reportWebVitals from './reportWebVitals'
import Home from './Home'
import '@fontsource/roboto/300.css'
import '@fontsource/roboto/400.css'
import '@fontsource/roboto/500.css'
import '@fontsource/roboto/700.css'

ReactDOM.render(
  <React.StrictMode>
    <CssBaseline />
    <Home />
  </React.StrictMode>,
  document.getElementById('root'),
)

// eslint-disable-next-line no-console
console.log('rendering in root')

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals()
