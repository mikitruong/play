import { Grid } from '@mui/material'
import React, { MutableRefObject, useRef } from 'react'
import {
  CartesianGrid, Legend, Line, LineChart, Tooltip, XAxis, YAxis,
} from 'recharts'
import useIntersection from './useIntersection'

const data = [
  {
    name: 'Page A',
    uv: 4000,
    pv: 2400,
    amt: 2400,
  },
  {
    name: 'Page B',
    uv: 3000,
    pv: 1398,
    amt: 2210,
  },
  {
    name: 'Page C',
    uv: 2000,
    pv: 9800,
    amt: 2290,
  },
  {
    name: 'Page D',
    uv: 2780,
    pv: 3908,
    amt: 2000,
  },
  {
    name: 'Page E',
    uv: 1890,
    pv: 4800,
    amt: 2181,
  },
  {
    name: 'Page F',
    uv: 2390,
    pv: 3800,
    amt: 2500,
  },
  {
    name: 'Page G',
    uv: 3490,
    pv: 4300,
    amt: 2100,
  },
]

const calculateAverageWage = (wages: number[]): number => {
  const totalWage = wages.reduce(
    (previousValue: number, currentValue: number) => previousValue + currentValue,
    0,
  )

  return totalWage / wages.length
}

const EPDChart: React.FC = () => {
  const lineChart = (
    <LineChart
      width={500}
      height={300}
      data={data}
      margin={{
        top: 5,
        right: 30,
        left: 20,
        bottom: 5,
      }}
    >
      <CartesianGrid strokeDasharray="3 3" />
      <XAxis dataKey="name" />
      <YAxis />
      <Tooltip />
      <Legend />
      <Line type="monotone" dataKey="pv" stroke="#8884d8" activeDot={{ r: 8 }} />
      <Line type="monotone" dataKey="uv" stroke="#82ca9d" />
    </LineChart>
  )

  const ref: MutableRefObject<HTMLDivElement | null> = useRef<HTMLDivElement>(null)

  const inViewport: Boolean = useIntersection(ref.current, '0px') // Trigger as soon as the element becomes visible

  return (
    <Grid container ref={ref} direction="column" alignContent="center">
      <Grid
        item
        sx={[{
          opacity: 0,
          transform: 'translateY(-20vh)',
          visibility: 'hidden',
          transition: 'opacity 0.6s ease-out, transform 1.2s ease-out',
          willChange: 'opacity, visibility',
        },
        inViewport && {
          opacity: 1,
          transform: 'none',
          visibility: 'visible',
        },
        ]}
      >
        {lineChart}
      </Grid>
    </Grid>
  )
}

export default EPDChart
