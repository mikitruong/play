import { Grid, Paper } from '@mui/material'
import React from 'react'
import Content from './Content'
import SideBar from './SideBar'
import TopBar from './TopBar'

const Home: React.FC = () => {
  return (
    <Paper
      style={{
        width: '100%',
        height: '100%',
        position: 'fixed',
        padding: 0,
        margin: 0,

        top: 0,
        left: 0,
      }}
      elevation={0}
      square
    >
      <Grid container direction="column" spacing={0} sx={{ width: 'inherit', height: 'inherit', flexWrap: 'nowrap' }}>
        <Grid item sx={{ flexGrow: 0 }}>
          <TopBar />
        </Grid>
        <Grid item container direction="row" spacing={0} sx={{ flexGrow: 1, flexWrap: 'nowrap', overflow: 'auto' }}>
          <Grid
            item
            sx={{
              flexGrow: 0,
              minHeight: 0,
              minWidth: 0,
              overflow: 'auto',
            }}
          >
            <SideBar />
          </Grid>
          <Grid
            item
            sx={{
              flexGrow: 1, overflow: 'auto', flexWrap: 'nowrap',
            }}
          >
            <Content />
          </Grid>
        </Grid>
      </Grid>
    </Paper>
  )
}

export default Home
