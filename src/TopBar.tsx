import { GitHub } from '@mui/icons-material'
import {
  Grid, IconButton, Paper, Tooltip, Typography,
} from '@mui/material'
import React from 'react'

const TopBar: React.FC = () => {
  return (
    <Paper
      elevation={0}
      square
      sx={{
        borderBottom: '1px solid',
        borderColor: 'divider',
      }}
    >
      <Grid container direction="row" justifyContent="flex-start">
        <Grid
          item
          container
          direction="row"
          justifyContent="flex-start"
          alignContent="center"
          xs
          style={{
            padding: '4px',
          }}
        >
          <Grid item container direction="row" xs>
            <Typography variant="h6" sx={{ color: 'error.main' }}>
              Equal Pay
            </Typography>
            <Typography variant="h6" sx={{ paddingLeft: 1 }}>
              | Day ,-
            </Typography>
          </Grid>
        </Grid>
        <Grid
          item
          container
          direction="row"
          justifyContent="flex-end"
          xs
          style={{
            flexGrow: 0,
            padding: '4px',
          }}
        >
          <Grid item xs>
            <Tooltip title="Gitlab Repository">
              <IconButton
                sx={{
                  border: '1px solid',
                  borderColor: 'divider',
                  borderRadius: '10px',
                  color: 'primary.main',
                }}
                href="https://gitlab.com/mikitruong/play"
                target="_blank"
              >
                <GitHub />
              </IconButton>
            </Tooltip>
          </Grid>
        </Grid>
      </Grid>
    </Paper>
  )
}

export default TopBar
