import React from 'react'

import Home from './Home'

const App: React.FC = () => {
  return (
    <div className="App">
      <header className="App-header">
        <Home />
      </header>
    </div>
  )
}

export default App
